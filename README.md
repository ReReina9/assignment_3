# Installazione Progetto


## Download

Scaricare e decomprimere il progetto

## Creazione DB
Si suppone venga utilizzato **Pg Admin 4**:
1.	**Creare** un nuovo DB con nome qualsiasi (es: Libreria).
2.	Cliccare con il tasto destro del mouse sul DB appena creato >> **Restore**
3.	Il parametro **Format** dovrebbe essere impostato su **Custom o Tar**
4.	In **File name** selezionare il percorso **"cartella_di_progetto/db"** dove `cartella_di_progetto` è l’indirizzo in cui è stata salvata la directory scaricata e il file **LibreriaDB** (In caso non si riuscisse a trovare il documento, selezionare in basso a destra “Format: All Files”).  
5.	Cliccare su **Select** e poi **Restore**.
6.	Nel DB dovrebbero essere state create 6 tabelle:

        libro
        autore
        casa_editrice
        genere
        autore_libro
        genere_libro

## Eclipse
Una volta creato il DB è possibile importare in eclipse l'applicativo
1.	Avviare Eclipse.
2.	Selezione **File** >> **Import** >> **Maven** >> **Existing Maven Projects**.
3.	In *Root Directory* selezione il percorso **"cartella_di_progetto/application"** dove `cartella_di_progetto` è l’indirizzo in cui è stata salvata la directory scaricata.
In “Projects” selezionare il file **/pom.xml** e poi cliccare su *Finish*.
 
4.	Dovrebbe quindi essere stato creato un progetto chiamato `LucaReina797754`.
5.	E’ necessario ora modificare le credenziali d’accesso al DB generato nel punto 2. 
Per fare ciò andare nella cartella **src/main/reosurces** >> **META-INF** e poi aprire il file **persistance.xml**
6.	Selezionare (se non già selezionato) il pannello **Source** per visualizzare il codice del file.
```
<persistence xmlns="http://java.sun.com/xml/ns/persistence"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd"
	version="2.0">
	<persistence-unit name="entityManager">
		<provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
        <class>com.assignment3.model.Genere</class>
        <class>com.assignment3.model.CasaEditrice</class>
        <class>com.assignment3.model.Autore</class>
        <class>com.assignment3.model.Libro</class>
		<properties>
			<property name="javax.persistence.jdbc.driver" value="org.postgresql.Driver" />
			<property name="javax.persistence.jdbc.url" value="jdbc:postgresql://localhost:5432/Libreria" />
			<property name="javax.persistence.jdbc.user" value="postgres" />
			<property name="javax.persistence.jdbc.password" value="pswd" />
			<property name="hibernate.show_sql" value="true" />
		</properties>
	</persistence-unit>
</persistence>

```
7.	Nella Property `javax.persistance.jdbc.url` modificare il `value` con l’opportuno **indirizzo** e **nome** del DB.
8.	Nella Property `javax.persistance.jdbc.user` modificare il `value` con l’opportuno **nome utente** impostato in postgresql.
9.	Nella Property `javax.persistance.jdbc.password` modificare il `value` con l’opportuna **password utente** impostata in postgresql.
 
10.	E’ possibile ora avviare i test contenuti nel package **src/test/java/com.assignment3**