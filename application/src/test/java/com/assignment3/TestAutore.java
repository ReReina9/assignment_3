package com.assignment3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.assignment3.service.Libreria;
import com.assignment3.model.Autore;
import com.assignment3.model.Libro;


public class TestAutore {

	private static Libreria libreria = new Libreria();
	
	@BeforeClass
	public static void Before() {
		libreria.deleteAll();
		
		Autore a1 = new Autore("Charles", "Dickens", 'M');
		libreria.add(a1);
		
		Autore a2 = new Autore("John Ronald Reuel", "Tolkien", 'M', 1892, 1, 3, "Italiana");
		libreria.add(a2);
		
		Calendar date = Calendar.getInstance();
		date.set(1979, 9, 22);
		Autore a3 = new Autore("Roberto", "Saviano", 'M', date , "Italiana");
		libreria.add(a3);
		
		libreria.save();
		
	}

	@Test
	public void Create() {
		Autore a = new Autore();
		a.setNome("Robin");
		a.setCognome("Hobb");
		a.setGenere('F');
		a.setYoB(1952, 3, 5);
		a.setNazionalita("Americana");
		
		libreria.add(a);
		libreria.save();
		
		assertEquals(a,libreria.findAutore(a.getId()));
		assertEquals(a.getNome(),libreria.findAutore(a.getId()).getNome());
		assertEquals(a.getCognome(),libreria.findAutore(a.getId()).getCognome());
		assertEquals(a.getGenere(),libreria.findAutore(a.getId()).getGenere());
		assertEquals(a.getYoB(),libreria.findAutore(a.getId()).getYoB());
		assertEquals(a.getNazionalita(),libreria.findAutore(a.getId()).getNazionalita());
	}
	
	@Test
	public void Read() {
		List<Autore> autori = libreria.findAllAutore();
	
		for(Autore autore : autori) {
			assertNotEquals(autore.getNome(), null);
			assertNotEquals(autore.getCognome(), null);
			assertNotEquals(autore.getGenere(), null);
		}
	}

	@Test
	public void Update() {
		Autore aupdate = libreria.findWhereNomeAndCognomeEquals_Autore("Roberto", "Saviano");
		aupdate.setNome("Update");
		libreria.update(aupdate);
		
		libreria.save();
		
		assertEquals(aupdate,libreria.findAutore(aupdate.getId()));
		assertEquals(libreria.findWhereNomeAndCognomeEquals_Autore("Roberto", "Saviano"), null);
	}
	
	@Test
	public void Delete() {
		Autore adelete = libreria.findWhereNomeAndCognomeEquals_Autore("Charles", "Dickens");
		libreria.delete(adelete);
		
		libreria.save();
		
		assertEquals(null, libreria.findAutore(adelete.getId()));
		assertNotEquals(adelete, libreria.findAutore(adelete.getId()));
	}
	
	@Test
	public void Relation() {
		Libro l1 = new Libro("Il Signore degli Anelli", "La Compagnia dell'Anello", "2013", 9.99, "Italiano");
		Libro l2 = new Libro("Il Signore degli Anelli", "Le Due Torri", "2013", 10.99, "Italiano");
		Libro l3 = new Libro("Il Signore degli Anelli", "Il Ritorno del Re", "2013", 10.99, "Italiano");
		
		Autore a = libreria.findWhereNomeAndCognomeEquals_Autore("John Ronald Reuel", "Tolkien");

		libreria.add(a);
		libreria.add(l1);
		libreria.add(l2);
		libreria.add(l3);
		
		libreria.addAutoreToLibro(a, l1);
		libreria.addAutoreToLibro(a, l2);
		libreria.addAutoreToLibro(a, l3);
		
		libreria.save();
		
		assertEquals(a.getLibri().get(0).toString(), libreria.findAllLibro().get(0).toString());
		assertEquals(a.getLibri().get(1).toString(), libreria.findAllLibro().get(1).toString());
		assertEquals(a.getLibri().get(2).toString(), libreria.findAllLibro().get(2).toString());
		
		libreria.delete(l1);
		libreria.save();
		
		assertEquals(2, a.getLibri().size());
		assertEquals(a.getLibri().get(0).toString(), l2.toString());
		assertEquals(a.getLibri().get(1).toString(), l3.toString());
		
		l3.setPrezzo(15.99);
		libreria.update(l3);
		libreria.save();
		
		assertEquals(a.getLibri().get(1).getPrezzo(), l3.getPrezzo());
		
		libreria.deleteAllLibro();
		libreria.save();
		
		assertTrue(a.getLibri().isEmpty());
	}
	
	@AfterClass
	public static void After() {
		libreria.deleteAll();
		
		libreria.close();
	}
}
