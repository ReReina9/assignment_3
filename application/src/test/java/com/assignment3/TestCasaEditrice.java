package com.assignment3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.assignment3.service.Libreria;
import com.assignment3.model.CasaEditrice;
import com.assignment3.model.Libro;


public class TestCasaEditrice {

	private static Libreria libreria = new Libreria();
	
	@BeforeClass
	public static void Before() {
		libreria.deleteAll();
		
		CasaEditrice ce1 = new CasaEditrice("Mondadori", "Segrate", "Italiana");
		libreria.add(ce1);
		
		CasaEditrice ce2 = new CasaEditrice("Einaudi", "Segrate", "Italiana");
		libreria.add(ce2);
		
		CasaEditrice ce3 = new CasaEditrice("Cambridge University Press");
		libreria.add(ce3);
		
		libreria.save();
	}
	
	@Test
	public void Create() {
		CasaEditrice ce = new CasaEditrice();
		ce.setNome("Mercure de France");
		ce.setSede("Parigi");
		ce.setNazionalita("Francese");
		
		libreria.add(ce);
		libreria.save();
		
		assertEquals(ce,libreria.findCasaEditrice(ce.getId()));
		assertEquals(ce.getNome(),libreria.findCasaEditrice(ce.getId()).getNome());
		assertEquals(ce.getSede(),libreria.findCasaEditrice(ce.getId()).getSede());
		assertEquals(ce.getNazionalita(),libreria.findCasaEditrice(ce.getId()).getNazionalita());
	}
	
	@Test
	public void Read() {
		List<CasaEditrice> case_editrici = libreria.findAllCasaEditrice();
	
		for(CasaEditrice casa_editrice : case_editrici) {
			assertNotEquals(casa_editrice.getNome(), null);
		}
	}
	
	@Test
	public void Update() {
		CasaEditrice ceupdate = libreria.findWhereNomeEquals_CasaEditrice("Einaudi");
		ceupdate.setNome("Update");
		libreria.update(ceupdate);
		
		libreria.save();
		
		assertEquals(ceupdate,libreria.findCasaEditrice(ceupdate.getId()));
		assertNotEquals(ceupdate, libreria.findWhereNomeEquals_CasaEditrice("Einaudi"));
	}
	
	@Test
	public void Delete() {
		CasaEditrice cedelete = libreria.findWhereNomeEquals_CasaEditrice("Cambridge University Press");
		libreria.delete(cedelete);
		
		libreria.save();
		
		assertEquals(null, libreria.findCasaEditrice(cedelete.getId()));
		assertNotEquals(cedelete, libreria.findCasaEditrice(cedelete.getId()));
	}
	
	@Test
	public void Relation() {
		Libro l1 = new Libro("Il Signore degli Anelli", "La Compagnia dell'Anello", "2013", 9.99, "Italiano");
		Libro l2 = new Libro("Il Signore degli Anelli", "Le Due Torri", "2013", 10.99, "Italiano");
		Libro l3 = new Libro("Il Signore degli Anelli", "Il Ritorno del Re", "2013", 10.99, "Italiano");
		
		CasaEditrice ce = libreria.findWhereNomeEquals_CasaEditrice("Mondadori");

		libreria.add(ce);
		libreria.add(l1);
		libreria.add(l2);
		libreria.add(l3);
		
		libreria.addLibroToCasaEditrice(ce, l1);
		libreria.addLibroToCasaEditrice(ce, l2);
		libreria.addLibroToCasaEditrice(ce, l3);
		
		libreria.save();
		
		assertEquals(ce.getLibri().get(0).toString(), libreria.findAllLibro().get(0).toString());
		assertEquals(ce.getLibri().get(1).toString(), libreria.findAllLibro().get(1).toString());
		assertEquals(ce.getLibri().get(2).toString(), libreria.findAllLibro().get(2).toString());
		
		libreria.delete(l1);
		libreria.save();

		assertEquals(2, ce.getLibri().size());
		assertEquals(ce.getLibri().get(0).toString(), l2.toString());
		assertEquals(ce.getLibri().get(1).toString(), l3.toString());
		
		l3.setPrezzo(15.99);
		libreria.update(l3);
		libreria.save();
		
		assertEquals(ce.getLibri().get(1).getPrezzo(), libreria.findAllLibro().get(1).getPrezzo());
		
		libreria.deleteAllLibro();
		libreria.save();
		
		assertTrue(ce.getLibri().isEmpty());
	}
	
	@AfterClass
	public static void After() {
		libreria.deleteAll();
		
		libreria.close();
	}
}
