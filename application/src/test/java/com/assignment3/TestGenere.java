package com.assignment3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.assignment3.service.Libreria;
import com.assignment3.model.Genere;
import com.assignment3.model.Libro;


public class TestGenere {

	private static Libreria libreria = new Libreria();
	
	@BeforeClass
	public static void Before() {
		libreria.deleteAll();
		
		Genere g1 = new Genere("Giallo");
		libreria.add(g1);
		
		Genere g2 = new Genere("Fantasy");
		libreria.add(g2);
		
		Genere g3 = new Genere("Thriller");
		libreria.add(g3);
		
		libreria.save();
		
	}

	@Test
	public void Create() {
		Genere g = new Genere("Azione");
		libreria.add(g);
		
		libreria.save();
		
		assertEquals(g,libreria.findGenere(g.getId()));
	}
	
	@Test
	public void Read() {
		List<Genere> generi = libreria.findAllGenere();
	
		for(Genere genere : generi) {
			assertNotEquals(genere.getNome(), null);
		}
	}

	@Test
	public void Update() {
		Genere gupdate = libreria.findWhereNomeEquals_Genere("Giallo");
		gupdate.setNome("Update");
		libreria.update(gupdate);
		
		libreria.save();
		
		assertEquals(gupdate,libreria.findGenere(gupdate.getId()));
		assertNotEquals(gupdate, libreria.findWhereNomeEquals_Genere("Azione"));
	}
	
	@Test
	public void Delete() {
		Genere gdelete = libreria.findWhereNomeEquals_Genere("Thriller");
		libreria.delete(gdelete);
		
		libreria.save();
		
		assertEquals(null, libreria.findGenere(gdelete.getId()));
		assertNotEquals(gdelete, libreria.findGenere(gdelete.getId()));
	}
	
	@Test
	public void Relation() {
		Libro l1 = new Libro("Il Signore degli Anelli", "La Compagnia dell'Anello", "2013", 9.99, "Italiano");
		Libro l2 = new Libro("Il Signore degli Anelli", "Le Due Torri", "2013", 10.99, "Italiano");
		Libro l3 = new Libro("Il Signore degli Anelli", "Il Ritorno del Re", "2013", 10.99, "Italiano");
		
		Genere g = libreria.findWhereNomeEquals_Genere("Fantasy");

		libreria.add(g);
		libreria.add(l1);
		libreria.add(l2);
		libreria.add(l3);
		
		libreria.addGenereToLibro(g, l1);
		libreria.addGenereToLibro(g, l2);
		libreria.addGenereToLibro(g, l3);
		
		libreria.save();
		
		assertEquals(g.getLibri().get(0).toString(), libreria.findAllLibro().get(0).toString());
		assertEquals(g.getLibri().get(1).toString(), libreria.findAllLibro().get(1).toString());
		assertEquals(g.getLibri().get(2).toString(), libreria.findAllLibro().get(2).toString());
		
		libreria.delete(l1);
		libreria.save();
		
		assertEquals(2, g.getLibri().size());
		assertEquals(g.getLibri().get(0).toString(), l2.toString());
		assertEquals(g.getLibri().get(1).toString(), l3.toString());
		
		l3.setPrezzo(15.99);
		libreria.update(l3);
		libreria.save();
		
		assertEquals(g.getLibri().get(1).getPrezzo(), libreria.findAllLibro().get(1).getPrezzo());
		
		libreria.deleteAllLibro();
		libreria.save();
		
		assertTrue(g.getLibri().isEmpty());
	}
	
	@AfterClass
	public static void After() {
		libreria.deleteAll();
		
		libreria.close();
	}
}
