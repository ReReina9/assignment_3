package com.assignment3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.assignment3.service.Libreria;
import com.assignment3.model.Autore;
import com.assignment3.model.CasaEditrice;
import com.assignment3.model.Genere;
import com.assignment3.model.Libro;


public class TestLibro {

	private static Libreria libreria = new Libreria();
	
	
	@BeforeClass
	public static void Before() {
		libreria.deleteAll();
		
		Libro l1 = new Libro("Il Signore degli Anelli", "La Compagnia dell'Anello", "2013", 9.99, "Italiano");
		libreria.add(l1);
		
		Libro l2 = new Libro("Il Signore degli Anelli", "Le Due Torri", "2013", 10.99, "Italiano");
		libreria.add(l2);
		
		Libro l3 = new Libro("Il Signore degli Anelli", "Il Ritorno del Re", "2013", 10.99, "Italiano");
		libreria.add(l3);
		
		libreria.save();
	}
	
	@Test
	public void Create() {
		Libro l = new Libro();
		l.setTitolo("Lo Hobbit");
		l.setSottotitolo("Un viaggio inaspettato");
		l.setEdizione("2015");
		l.setPrezzo(15.99);
		l.setLingua("Italiano");
		
		libreria.add(l);
		libreria.save();
		
		assertEquals(l,libreria.findLibro(l.getId()));
		assertEquals(l.getTitolo(),libreria.findLibro(l.getId()).getTitolo());
		assertEquals(l.getSottotitolo(),libreria.findLibro(l.getId()).getSottotitolo());
		assertEquals(l.getEdizione(),libreria.findLibro(l.getId()).getEdizione());
		assertEquals(l.getPrezzo(),libreria.findLibro(l.getId()).getPrezzo());
		assertEquals(l.getLingua(),libreria.findLibro(l.getId()).getLingua());
	}
	
	@Test
	public void Read() {
		List<Libro> libri = libreria.findAllLibro();
	
		for(Libro libro : libri) {
			assertNotEquals(libro.getTitolo(), null);
		}
	}

	@Test
	public void Update() {
		List<Libro> ls = libreria.findWhereTitoloEquals_Libro("Il Signore degli Anelli");
		Libro lupdate = ls.get(0);
		lupdate.setTitolo("Update");
		libreria.update(lupdate);
		
		libreria.save();
		
		assertEquals(lupdate, libreria.findLibro(lupdate.getId()));
	}
	
	@Test
	public void Delete() {
		List<Libro> ls = libreria.findWhereTitoloEquals_Libro("Il Signore degli Anelli");
		Libro ldelete = ls.get(0);
		libreria.delete(ldelete);
		
		libreria.save();
		
		assertEquals(null, libreria.findCasaEditrice(ldelete.getId()));
		assertNotEquals(ldelete, libreria.findCasaEditrice(ldelete.getId()));
	}
	
	@Test
	public void Relation() {
		Genere g1 = new Genere("Fantasy");
		Genere g2 = new Genere("Avventura");
		CasaEditrice ce = new CasaEditrice("Mondadori", "Segrate", "Italiana");
		Autore a1 = new Autore("John R. R.", "Tolkien", 'M', 1892, 1, 3, "Italiana");
		Autore a2 = new Autore("John Ronald Reuel", "Tolkien", 'M', 1892, 1, 3, "Italiana");
		Libro lseq = new Libro("Il Signore degli Anelli", "Il Ritorno del Re", "2013", 10.99, "Italiano");
				
		List<Libro> ls = libreria.findWhereTitoloEquals_Libro("Il Signore degli Anelli");
		Libro l = ls.get(0);

		libreria.add(l);
		libreria.add(lseq);
		libreria.add(g1);
		libreria.add(g2);
		libreria.add(a1);
		libreria.add(a2);
		
		libreria.addSequelToLibro(l, lseq);
		libreria.addGenereToLibro(g1, l);
		libreria.addGenereToLibro(g2, l);
		libreria.addAutoreToLibro(a1, l);
		libreria.addAutoreToLibro(a2, l);
		libreria.addLibroToCasaEditrice(ce, l);
		
		libreria.save();
		
		assertEquals(l.getGeneri().get(0).toString(), libreria.findAllGenere().get(0).toString());
		assertEquals(l.getGeneri().get(1).toString(), libreria.findAllGenere().get(1).toString());
		assertEquals(l.getAutori().get(0).toString(), libreria.findAllAutore().get(0).toString());
		assertEquals(l.getAutori().get(1).toString(), libreria.findAllAutore().get(1).toString());
		assertEquals(l.getCasa_editrice().toString(), libreria.findAllCasaEditrice().get(0).toString());
		assertEquals(l.getSequel().toString(), libreria.findLibro(lseq.getId()).toString());
		
		libreria.delete(g2);
		libreria.delete(a2);
		libreria.save();
		
		assertEquals(1, l.getGeneri().size());
		assertEquals(1, l.getAutori().size());
		
		g1.setNome("Avventura");
		libreria.update(g1);
		a1.setNome("John R. R.");
		libreria.update(a1);
		libreria.save();
		
		assertEquals(l.getGeneri().get(0).getNome(), libreria.findGenere(g1.getId()).getNome());
		assertEquals(l.getAutori().get(0).getNome(), libreria.findAutore(a1.getId()).getNome());
		
		libreria.deleteAllGenere();
		libreria.deleteAllAutore();
		libreria.deleteAllCasaEditrice();
		libreria.save();
		
		assertTrue(l.getGeneri().isEmpty());
		assertTrue(l.getAutori().isEmpty());
		assertEquals(l.getCasa_editrice(), null);
	}	
	
	@AfterClass
	public static void After() {
		libreria.deleteAll();
		
		libreria.close();
	}
}