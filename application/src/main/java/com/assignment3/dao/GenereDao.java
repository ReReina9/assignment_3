package com.assignment3.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.assignment3.model.Genere;
import com.assignment3.model.Libro;

public class GenereDao {
	public GenereDao() {
	}
	
	public void persist(Genere entity, EntityManager em) {
		em.persist(entity);
	}
	
	public void update(Genere entity, EntityManager em) {
		em.merge(entity);
	}
	
	public Genere find(Integer id, EntityManager em) {
		Genere entity = (Genere) em.find(Genere.class, id);
		return entity;
	}
	
	public Genere findWhereNomeEquals(String s, EntityManager em) {
		Genere g = null;
		List<Genere> entities = findAll(em);
		for (Genere entity : entities) {
			if(entity.getNome().equalsIgnoreCase(s)) {
				g = (Genere) em.find(Genere.class, entity.getId());
			}
		}
		return g;
	}
	
	public void delete(Genere entity, EntityManager em) {
		List<Libro> ls = entity.getLibri();
		if(ls != null) {
			for(Libro l : ls) {
				l.getGeneri().remove(entity);
				em.merge(l);
			}
		}		
		em.remove(entity);
	}	
	
	public List<Genere> findAll(EntityManager em) {
		List<Genere> entities = (List<Genere>) em.createQuery("SELECT g FROM Genere g",Genere.class).getResultList();
		return entities;
	}
	
	public void deleteAll(EntityManager em) {
		List<Genere> entities = findAll(em);
		for (Genere entity : entities) {
			delete(entity, em);
		}
	}
}
