package com.assignment3.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.assignment3.model.Autore;
import com.assignment3.model.CasaEditrice;
import com.assignment3.model.Genere;
import com.assignment3.model.Libro;

public class LibroDao {
	public LibroDao() {
	}
	
	public void persist(Libro entity, EntityManager em) {
		em.persist(entity);
	}
	
	public void update(Libro entity, EntityManager em) {
		em.merge(entity);
	}
	
	public Libro find(Integer id, EntityManager em) {
		Libro entity = (Libro) em.find(Libro.class, id);
		return entity;
	}
	
	public List<Libro> findWhereTitoloEquals_Libro(String titolo, EntityManager em) {
		List<Libro> entities = findAll(em);
		List<Libro> found = new ArrayList<>();
		for (Libro entity : entities) {
			if(entity.getTitolo().equalsIgnoreCase(titolo)) {
				found.add(entity);
			}
		}
		return found;
	}
	
	public void delete(Libro entity, EntityManager em) {
		List<Genere> gs = entity.getGeneri();
		if(gs != null) {
			for(Genere g : gs) {
				g.getLibri().remove(entity);
				em.merge(g);
			}
		}
		List<Autore> as = entity.getAutori();
		if(as != null) {
			for(Autore a : as) {
				a.getLibri().remove(entity);
				em.merge(a);
			}
		}
		CasaEditrice ce = entity.getCasa_editrice();
		if(ce != null) {
			ce.getLibri().remove(entity);
			em.merge(ce);
		}
		em.remove(entity);
	}	
	
	public List<Libro> findAll(EntityManager em) {
		List<Libro> entities = (List<Libro>) em.createQuery("SELECT l FROM Libro l",Libro.class).getResultList();
		return entities;
	}
	
	public void deleteAll(EntityManager em) {
		List<Libro> entities = findAll(em);
		for (Libro entity : entities) {
			delete(entity, em);
		}
	}
}
