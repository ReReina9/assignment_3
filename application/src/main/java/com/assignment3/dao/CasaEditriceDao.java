package com.assignment3.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.assignment3.model.CasaEditrice;
import com.assignment3.model.Libro;

public class CasaEditriceDao {
	public CasaEditriceDao() {
	}
	
	public void persist(CasaEditrice entity, EntityManager em) {
		em.persist(entity);
	}
	
	public void update(CasaEditrice entity, EntityManager em) {
		em.merge(entity);
	}
	
	public CasaEditrice find(Integer id, EntityManager em) {
		CasaEditrice entity = (CasaEditrice) em.find(CasaEditrice.class, id);
		return entity;
	}
	
	public CasaEditrice findWhereNomeEquals(String s, EntityManager em) {
		CasaEditrice ce = null;
		List<CasaEditrice> entities = findAll(em);
		for (CasaEditrice entity : entities) {
			if(entity.getNome().equalsIgnoreCase(s)) {
				ce = (CasaEditrice) em.find(CasaEditrice.class, entity.getId());
			}
		}
		return ce;
	}
	
	public void delete(CasaEditrice entity, EntityManager em) {
		List<Libro> ls = entity.getLibri();
		if(ls != null) {
			for(Libro l : ls) {
				l.setCasa_editrice(null);
				em.merge(l);
			}
		}	
		em.remove(entity);
	}	
	
	public List<CasaEditrice> findAll(EntityManager em) {
		List<CasaEditrice> entities = (List<CasaEditrice>) em.createQuery("SELECT ce FROM CasaEditrice ce",CasaEditrice.class).getResultList();
		return entities;
	}
	
	public void deleteAll(EntityManager em) {
		List<CasaEditrice> entities = findAll(em);
		for (CasaEditrice entity : entities) {
			delete(entity, em);
		}
	}

}
