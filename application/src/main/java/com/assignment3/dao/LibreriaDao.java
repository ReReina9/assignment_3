package com.assignment3.dao;

import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.assignment3.dao.AutoreDao;
import com.assignment3.dao.CasaEditriceDao;
import com.assignment3.dao.GenereDao;
import com.assignment3.dao.LibroDao;
import com.assignment3.model.Autore;
import com.assignment3.model.CasaEditrice;
import com.assignment3.model.Genere;
import com.assignment3.model.Libro;

import org.apache.log4j.PropertyConfigurator;

public class LibreriaDao {
	private static EntityManager em;
	private static EntityManagerFactory factory;
	
	private static Libro libro = new Libro();
	private static Autore autore = new Autore();
	private static CasaEditrice casa_editrice = new CasaEditrice();
	private static Genere genere = new Genere();
	
	private static LibroDao libro_dao = new LibroDao();
	private static AutoreDao autore_dao = new AutoreDao();
	private static CasaEditriceDao casa_editrice_dao = new CasaEditriceDao();
	private static GenereDao genere_dao = new GenereDao();
	
	private static final String persistenceUnitName="entityManager";
	
	public LibreriaDao() {
		setLoggerProperty();
	}
	
	public void init() {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
		em = factory.createEntityManager();
	}
	
	public void close() {
		em.close();
		factory.close();
	}
	
	public void transaction() {
		em.getTransaction().begin();
		em.getTransaction().commit();
	}
	
	public void persist(Object obj) {
		if(obj.getClass() == libro.getClass()) {
			libro_dao.persist((Libro)obj, em);
		}
		else if(obj.getClass() == autore.getClass()) {
			autore_dao.persist((Autore)obj, em);
		}
		else if(obj.getClass() == casa_editrice.getClass()) {
			casa_editrice_dao.persist((CasaEditrice)obj, em);
		}
		else if(obj.getClass() == genere.getClass()) {
			genere_dao.persist((Genere)obj, em);
		}
	}
	
	public void update(Object obj) {
		if(obj.getClass() == libro.getClass()) {
			libro_dao.update((Libro)obj, em);
		}
		else if(obj.getClass() == autore.getClass()) {
			autore_dao.update((Autore)obj, em);
		}
		else if(obj.getClass() == casa_editrice.getClass()) {
			casa_editrice_dao.update((CasaEditrice)obj, em);
		}
		else if(obj.getClass() == genere.getClass()) {
			genere_dao.update((Genere)obj, em);
		}
	}
	
	public void delete(Object obj) {
		if(obj.getClass() == libro.getClass()) {
			libro_dao.delete((Libro)obj, em);
		}
		else if(obj.getClass() == autore.getClass()) {
			autore_dao.delete((Autore)obj, em);
		}
		else if(obj.getClass() == casa_editrice.getClass()) {
			casa_editrice_dao.delete((CasaEditrice)obj, em);
		}
		else if(obj.getClass() == genere.getClass()) {
			genere_dao.delete((Genere)obj, em);
		}
	}	
	
	/*
	 * find
	 */
	public Libro findLibro(Integer id) {
		return libro_dao.find(id, em);
	}
	
	public Autore findAutore(Integer id) {
		return autore_dao.find(id, em);
	}
	
	public CasaEditrice findCasaEditrice(Integer id) {
		return casa_editrice_dao.find(id, em);
	}
	
	public Genere findGenere(Integer id) {
		return genere_dao.find(id, em);
	}
	
	/*
	 * findWhere
	 */
	public Genere findWhereNomeEquals_Genere(String nome) {
		return genere_dao.findWhereNomeEquals(nome, em);
	}
	
	public Autore findWhereNomeAndCognomeEquals_Autore(String nome, String cognome) {
		return autore_dao.findWhereNomeAndCognomeEquals(nome, cognome, em);
	}
	
	public CasaEditrice findWhereNomeEquals_CasaEditrice(String nome) {
		return casa_editrice_dao.findWhereNomeEquals(nome, em);
	}
	
	public List<Libro> findWhereTitoloEquals_Libro(String titolo) {
		return libro_dao.findWhereTitoloEquals_Libro(titolo, em);
	}
	
	/*
	 * findAll
	 */
	public List<Libro> findAllLibro() {
		return libro_dao.findAll(em);
	}
	
	public List<Autore> findAllAutore() {
		return autore_dao.findAll(em);
	}
	
	public List<CasaEditrice> findAllCasaEditrice() {
		return casa_editrice_dao.findAll(em);
	}
	
	public List<Genere> findAllGenere() {
		return genere_dao.findAll(em);
	}
	
	/*
	 * deleteAll
	 */
	public void deleteAllLibro() {
		libro_dao.deleteAll(em);
	}
	
	public void deleteAllAutore() {
		autore_dao.deleteAll(em);
	}
	
	public void deleteAllCasaEditrice() {
		casa_editrice_dao.deleteAll(em);
	}
	
	public void deleteAllGenere() {
		genere_dao.deleteAll(em);
	}
	
	private void setLoggerProperty() {
		Properties props = new Properties();
		try {
		props.load(this.getClass().getClassLoader().getResourceAsStream("META-INF/log4j.properties"));
		}catch(Exception e) {
			e.printStackTrace();
		}
		PropertyConfigurator.configure(props);
	}
	
	/*
	 * Relations @ManytoMany
	 */
	public void addGenereToLibro(Genere g, Libro l) {
		l.addGenere(g);
		g.addLibro(l);
		persist(g);
		persist(l);
	}
	
	public void addAutoreToLibro(Autore a, Libro l) {
		l.addAutore(a);
		a.addLibro(l);
		persist(a);
		persist(l);
	}

	public void addLibroToCasaEditrice(CasaEditrice ce, Libro l) {
		ce.addLibro(l);
		l.setCasa_editrice(ce);
		persist(ce);
		persist(l);
	}

	public void addSequelToLibro(Libro libro, Libro sequel) {
		libro.setSequel(sequel);
		persist(libro);
	}
}
