package com.assignment3.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.assignment3.model.Autore;
import com.assignment3.model.Libro;

public class AutoreDao {
	public AutoreDao() {
	}
	
	public void persist(Autore entity, EntityManager em) {
		em.persist(entity);
	}
	
	public void update(Autore entity, EntityManager em) {
		em.merge(entity);
	}
	
	public Autore find(Integer id, EntityManager em) {
		Autore entity = (Autore) em.find(Autore.class, id);
		return entity;
	}
	
	public Autore findWhereNomeAndCognomeEquals(String nome, String cognome, EntityManager em) {
		Autore g = null;
		List<Autore> entities = findAll(em);
		for (Autore entity : entities) {
			if(entity.getNome().equalsIgnoreCase(nome) && entity.getCognome().equalsIgnoreCase(cognome)) {
				g = (Autore) em.find(Autore.class, entity.getId());
			}
		}
		return g;
	}
	
	public void delete(Autore entity, EntityManager em) {
		List<Libro> ls = entity.getLibri();
		if(ls != null) {
			for(Libro l : ls) {
				l.getAutori().remove(entity);
				em.merge(l);
			}
		}
		em.remove(entity);
	}	
	
	public List<Autore> findAll(EntityManager em) {
		List<Autore> entities = (List<Autore>) em.createQuery("SELECT a FROM Autore a",Autore.class).getResultList();
		return entities;
	}
	
	public void deleteAll(EntityManager em) {
		List<Autore> entities = findAll(em);
		for (Autore entity : entities) {
			delete(entity, em);
		}
	}
}
