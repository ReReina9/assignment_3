package com.assignment3.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="libro")
public class Libro {
	
	@Id
	@Column(name = "id_libro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "casa_editrice_fk")
	private CasaEditrice casa_editrice;
	
	@ManyToMany(mappedBy = "libri")
	private List<Autore> autori = new ArrayList<>();
	
	@ManyToMany(mappedBy = "libri")
	private List<Genere> generi = new ArrayList<>();
	
	@JoinColumn(name = "sequel_fk")
	@OneToOne
	private Libro sequel;
	
	@Column(name = "titolo")
	private String titolo;
	
	@Column(name = "sottotitolo")
	private String sottotitolo;
	
	@Column(name = "edizione")
	private String edizione;
	
	@Column(name = "prezzo")
	private BigDecimal prezzo;
	
	@Column(name = "lingua")
	private String lingua;
	
	public Libro () {}
	
	public Libro(String titolo, String sottotitolo, String edizione, double prezzo, String lingua) {
		super();
		BigDecimal bd = new BigDecimal(prezzo);
		this.titolo = titolo;
		this.sottotitolo = sottotitolo;
		this.edizione = edizione;
		this.prezzo = bd;
		this.lingua = lingua;
	}

	public Integer getId() {
		return id;
	}

	public CasaEditrice getCasa_editrice() {
		return casa_editrice;
	}

	public void setCasa_editrice(CasaEditrice casa_editrice) {
		this.casa_editrice = casa_editrice;
	}

	public List<Autore> getAutori() {
		return autori;
	}

	public void addAutore(Autore autore) {
		this.autori.add(autore);
	}
	
	public List<Genere> getGeneri() {
		return generi;
	}

	public void addGenere(Genere genere) {
		this.generi.add(genere);
	}
	
	public Libro getSequel() {
		return sequel;
	}

	public void setSequel(Libro sequel) {
		this.sequel = sequel;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getSottotitolo() {
		return sottotitolo;
	}

	public void setSottotitolo(String sottotitolo) {
		this.sottotitolo = sottotitolo;
	}

	public String getEdizione() {
		return edizione;
	}

	public void setEdizione(String edizione) {
		this.edizione = edizione;
	}

	public BigDecimal getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		BigDecimal bd = new BigDecimal(prezzo);
		this.prezzo = bd;
	}

	public String getLingua() {
		return lingua;
	}

	public void setLingua(String lingua) {
		this.lingua = lingua;
	}

	@Override
	public String toString() {
		return "Libro [titolo=" + titolo + ", sottotitolo=" + sottotitolo + ", edizione=" + edizione + ", prezzo="
				+ prezzo.doubleValue() + ", lingua=" + lingua + "]";
	}
	
	
}