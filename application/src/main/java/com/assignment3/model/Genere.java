package com.assignment3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="genere")
public class Genere {
	
	@Id
	@Column(name = "id_genere")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nome")
	private String nome;
	
	@ManyToMany
    @JoinTable(
        name = "genere_libro", 
        joinColumns = { @JoinColumn(name = "id_genere") }, 
        inverseJoinColumns = { @JoinColumn(name = "id_libro") }
    )
	private List<Libro> libri = new ArrayList<>();
	
	public Genere() {}

	public Genere(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public List<Libro> getLibri() {
		return libri;
	}

	public void addLibro(Libro libro) {
		this.libri.add(libro);
	}
	
	@Override
	public String toString() {
		return "Genere [nome=" + nome + "]";
	}
}
