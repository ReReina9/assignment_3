package com.assignment3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="casa_editrice")
public class CasaEditrice {
	
	@Id
	@Column(name = "id_casa_editrice")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "sede")
	private String sede;
	
	@Column(name = "nazionalita")
	private String nazionalita;
	
	@OneToMany(mappedBy = "casa_editrice")
	private List<Libro> libri = new ArrayList<>();
	
	public CasaEditrice() {}
	
	public CasaEditrice(String nome) {
		super();
		this.nome = nome;
	}
	
	public CasaEditrice(String nome, String sede, String nazionalita) {
		super();
		this.nome = nome;
		this.sede = sede;
		this.nazionalita = nazionalita;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getNazionalita() {
		return nazionalita;
	}

	public void setNazionalita(String nazionalita) {
		this.nazionalita = nazionalita;
	}

	public List<Libro> getLibri() {
		return libri;
	}

	public void addLibro(Libro libro) {
		this.libri.add(libro);
	}
	
	@Override
	public String toString() {
		return "CasaEditrice [nome=" + nome + ", sede=" + sede + ", nazionalita=" + nazionalita + "]";
	}
}
