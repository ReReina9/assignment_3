package com.assignment3.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="autore")
public class Autore {
	
	@Id
	@Column(name = "id_autore")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "cognome")
	private String cognome;
	
	@Column(name = "genere")
	private char genere;
	
	@Column(name = "anno_di_nascita")
	private Date YoB;
	
	@Column(name = "nazionalita")
	private String nazionalita;
	
	
	@ManyToMany
    @JoinTable(
        name = "autore_libro", 
        joinColumns = { @JoinColumn(name = "id_autore") }, 
        inverseJoinColumns = { @JoinColumn(name = "id_libro") }
    )
	private List<Libro> libri = new ArrayList<>();
	
	public Autore() {}

	public Autore(String nome, String cognome, char genere) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.genere = genere;
	}

	public Autore(String nome, String cognome, char genere, Calendar date, String nazionalita) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.genere = genere;
		this.YoB = date.getTime();
		this.nazionalita = nazionalita;
	}
	
	public Autore(String nome, String cognome, char genere, int y, int m, int d, String nazionalita) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.genere = genere;
		Calendar date = Calendar.getInstance();
		date.set(y,m,d);
		this.YoB = date.getTime();
		this.nazionalita = nazionalita;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public char getGenere() {
		return genere;
	}

	public void setGenere(char genere) {
		this.genere = genere;
	}

	public Calendar getYoB() {
		Calendar date = Calendar.getInstance();
		date.setTime(this.YoB);
		return date;
	}

	public void setYoB(int y, int m, int d) {
		Calendar date = Calendar.getInstance();
		date.set(y, m, d);
		this.YoB = date.getTime();
	}

	public String getNazionalita() {
		return nazionalita;
	}

	public void setNazionalita(String nazionalita) {
		this.nazionalita = nazionalita;
	}
	
	public List<Libro> getLibri() {
		return libri;
	}
	
	public void addLibro(Libro libro) {
		this.libri.add(libro);
	}

	@Override
	public String toString() {
		return "Autore [nome=" + nome + ", cognome=" + cognome + ", genere=" + genere + ", YoB=" + YoB
				+ ", nazionalita=" + nazionalita + "]";
	}

	
}
	
