package com.assignment3.service;

import java.util.List;

import com.assignment3.dao.LibreriaDao;
import com.assignment3.model.Autore;
import com.assignment3.model.CasaEditrice;
import com.assignment3.model.Genere;
import com.assignment3.model.Libro;

public class Libreria {

	private static LibreriaDao libreriaDao = new LibreriaDao();
	
	public Libreria() {
		init();
	}

	private void init() {
		libreriaDao.init();
	}
	
	public void save() {
		libreriaDao.transaction();
	}
	
	public void close() {
		libreriaDao.close();
	}
	
	public void add(Object entity) {
		libreriaDao.persist(entity);
	}
	
	public void update(Object entity) {
		libreriaDao.update(entity);
	}
	
	public void delete(Object entity) {
		libreriaDao.delete(entity);
	}
	
	/*
	 * find
	 */
	public Libro findLibro(Integer id) {
		return libreriaDao.findLibro(id);
	}
	
	public Autore findAutore(Integer id) {
		return libreriaDao.findAutore(id);
	}
	
	public CasaEditrice findCasaEditrice(Integer id) {
		return libreriaDao.findCasaEditrice(id);
	}
	
	public Genere findGenere(Integer id) {
		return libreriaDao.findGenere(id);
	}
	
	/*
	 * findWhere
	 */
	public Genere findWhereNomeEquals_Genere(String s) {
		return libreriaDao.findWhereNomeEquals_Genere(s);
	}

	public Autore findWhereNomeAndCognomeEquals_Autore(String nome, String cognome) {
		return libreriaDao.findWhereNomeAndCognomeEquals_Autore(nome, cognome);
	}
	
	public CasaEditrice findWhereNomeEquals_CasaEditrice(String nome) {
		return libreriaDao.findWhereNomeEquals_CasaEditrice(nome);
	}
	
	public List<Libro> findWhereTitoloEquals_Libro(String titolo) {
		return libreriaDao.findWhereTitoloEquals_Libro(titolo);
	}
	
	/*
	 * findAll
	 */
	public List<Libro> findAllLibro() {
		return libreriaDao.findAllLibro();
	}
	
	public List<Autore> findAllAutore() {
		return libreriaDao.findAllAutore();
	}
	
	public List<CasaEditrice> findAllCasaEditrice() {
		return libreriaDao.findAllCasaEditrice();
	}
	
	public List<Genere> findAllGenere() {
		return libreriaDao.findAllGenere();
	}
	
	/*
	 * deleteAll
	 */
	public void deleteAll() {
		libreriaDao.deleteAllLibro();
		libreriaDao.deleteAllAutore();
		libreriaDao.deleteAllCasaEditrice();
		libreriaDao.deleteAllGenere();
		save();
	}
	
	public void deleteAllLibro() {
		libreriaDao.deleteAllLibro();
		save();
	}
	
	public void deleteAllAutore() {
		libreriaDao.deleteAllAutore();
		save();
	}
	
	public void deleteAllCasaEditrice() {
		libreriaDao.deleteAllCasaEditrice();
		save();
	}
	
	public void deleteAllGenere() {
		libreriaDao.deleteAllGenere();
		save();
	}
	
	/*
	 * Relations
	 */
	public void addGenereToLibro(Genere g, Libro l) {
		libreriaDao.addGenereToLibro(g, l);
	}
	
	public void addAutoreToLibro(Autore a, Libro l) {
		libreriaDao.addAutoreToLibro(a, l);
	}

	public void addLibroToCasaEditrice(CasaEditrice ce, Libro l) {
		libreriaDao.addLibroToCasaEditrice(ce, l);		
	}
	
	public void addSequelToLibro(Libro libro, Libro sequel) {
		libreriaDao.addSequelToLibro(libro, sequel);		
	}
}
